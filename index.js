
const express = require('express');
const db = require('./fileDb');
const products = require ('./app/products')
const app = express();


const port = 8000;

app.use(express.json());
app.use('/products', products);

db.init();

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);

});


const express = require('express');
const db = require('../fileDb')


const router = express.Router();



router.get('/', (req, res) => {
 const products = db.getItems();
 return res.send(products);
});

router.get('/:id', (req, res) => {
  const product = db.getItem(req.params.id);

  if(!product){
   return res.status(404).send({message: "Not found"});
  }

  res.send(product);
});

router.post('/', (req, res)=> {

  const product = {
    title: req.body.title,
    price: req.body.price,
    description: req.body.description,
  }


  db.addItem(product);

  return res.send('Created new product with ID' + id);
});

module.exports = router;

const someFunctions = {
    greet: () => console.log('Hello!'),
    greetName: name => console.log('Hello, ' + name)
};

module.exports = someFunctions;